package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Spike extends Pane {

    public static final int SPIKE_WIDTH = 20;
    public static final int SPIKE_HIGHT = 20;

    private int x;
    private int y;

    private Image spikeImg;
    private ImageView spikeView;

    public Spike(int x, int y){

        this.x=x;
        this.y=y;

        this.setTranslateX(x);
        this.setTranslateY(y);

        this.spikeImg = new Image(getClass().getResourceAsStream("/assets/Spike.png"));
        this.spikeView = new ImageView(spikeImg);
        this.spikeView.setFitHeight(SPIKE_HIGHT);
        this.spikeView.setFitWidth(SPIKE_WIDTH);

        this.getChildren().add(this.spikeView);

    }


}
