package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;


public class Ladder extends Pane {

    public static final int LADDER_WIDTH = 64;
    public static final int LADDER_HEIGHT = 128;

    private int x;
    private int y;

    private Image ladderImg;

    private ImageView ladderView;

    public Ladder(int x, int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.ladderImg = new Image(getClass().getResourceAsStream("/assets/ladder.png"));
        this.ladderView = new ImageView(ladderImg);
        this.ladderView.setFitWidth(LADDER_WIDTH);
        this.ladderView.setFitHeight(LADDER_HEIGHT);

        this.getChildren().add(this.ladderView);
    }

    public Image getLadderImg() {
        return ladderImg;
    }

    public ImageView getLadderView() {
        return ladderView;
    }

    public static int getLadderWidth() {
        return LADDER_WIDTH;
    }

    public static int getLadderHeight() {
        return LADDER_HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
