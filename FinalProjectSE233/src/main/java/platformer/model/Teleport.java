package platformer.model;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;



public class Teleport extends Pane {

    public static final int TELEPORT_WIDTH = 45;
    public static final int TELEPORT_HEIGHT = 82;

    private int x;
    private int y;

    private Image teleportImg;
    private ImageView teleportView;


    public Teleport(int x, int y) {

        this.x=x;
        this.y=y;

        this.setTranslateX(x);
        this.setTranslateY(y);

        this.teleportImg = new Image(getClass().getResourceAsStream("/assets/Teleport.png"));
        this.teleportView = new ImageView(teleportImg);
        this.teleportView.setFitWidth(TELEPORT_WIDTH);
        this.teleportView.setFitHeight(TELEPORT_HEIGHT);

        this.getChildren().add(this.teleportView);
    }


    public Image getTeleportImg() {
        return teleportImg;
    }

    public ImageView getTeleportView() {
        return teleportView;
    }

    public static int getTeleportWidth() {
        return TELEPORT_WIDTH;
    }

    public static int getTeleportHeight() {
        return TELEPORT_HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
