package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class KeyForOpenChest extends Pane {

    public static final int CHARACTER_WIDTH = 32;
    public static final int CHARACTER_HEIGHT = 32;

    private int x;
    private int y;

    private Image KeyImg;
    private ImageView imageView;

    public KeyForOpenChest(int x , int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.KeyImg = new Image(getClass().getResourceAsStream("/assets/keys.png"));

        this.imageView = new ImageView(KeyImg);
        this.getChildren().add(this.imageView);

        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
    }


    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }


}
