package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import platformer.view.Platform;

public class Character extends Pane {

    Logger logger = LoggerFactory.getLogger(Character.class);

    public static final int CHARACTER_WIDTH = 32;
    public static final int CHARACTER_HEIGHT = 32;

    private Image characterImg;
    private AnimatedSprite imageView;

    private int x;
    private int y;

    private int startX = 30;
    private int startY = 30;

    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode jumpKey;
    private KeyCode upKey;

    public void teleport() {
        x = (int) (Math.random() * 800 +1);
        y = (int) (Math.random() * 400 +1);
        falling = true;
        isTeleporting = true;
    }

    public void touchSpike() {
        respawn();

    }

    public void respawn() {

        x = startX;
        y = startY;
        imageView.setFitWidth(CHARACTER_WIDTH);
        imageView.setFitHeight(CHARACTER_HEIGHT);
        isMoveLeft = false;
        isMoveRight = false;
        falling = true;
        canJump = false;
        jumping = false;

    }

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 7;
    int yMaxVelocity = 17;

    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    boolean isTeleporting = false;

    boolean isOnLadder = false;
    boolean isOnBlock = false;
    boolean isClimbingUp = false;
    boolean isOnSpring = false;

    public Character(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode jumpKey, KeyCode upKey) {

        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/assets/MarioSheet.png"));
        this.imageView = new AnimatedSprite(characterImg,4,4,offsetX,offsetY,16,16);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.jumpKey = jumpKey;
        this.upKey = upKey;
        this.getChildren().addAll(this.imageView);
    }

    //Spring code
    public void intersectsWithSpring() {
        if (!isOnSpring){
            isOnSpring = true;
            falling = false;
        }
    }

    public void springJump() {
        if (isOnSpring()) {
            isOnSpring = false;
            falling = false;
        }
    }

    //End of Spring code

    //Climbing related methods
    public void climbUp() {
        isClimbingUp = true;
    }

    public void intersectsWithLadder() {
        if (!isOnLadder){
            isOnLadder = true;
            falling = false;
        }

    }

    public void exitLadder() {
        if (isOnLadder()){
            isOnLadder = false;
            falling =true;

        }

    }
    //End of Climbing related methods

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
        yVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            yVelocity = yMaxVelocity;
            canJump = false;
            jumping = true;
            falling = false;
        }
        //For Spring
        if (isOnSpring()){
            yVelocity=20;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
 		    y = Platform.GROUND - CHARACTER_HEIGHT;
            falling = false;
            canJump = true;
            yVelocity = 0;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }
    }

    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }
        if(isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    //Code for block colliding
    //4.Check collision with block
    public void checkCollided(Block block) {
        if (isMoveLeft && isOnBlock && !falling) {
            x = block.getX() + block.BLOCK_WIDTH + 1;
            stop();
        }
        if (isMoveRight && isOnBlock && !falling) {
            x = block.getX()  - CHARACTER_WIDTH - 1;
            stop();
        }

        if (isOnBlock && falling){
            y = block.getY() - CHARACTER_HEIGHT;
        }

        if (jumping && isOnBlock) {
            y = block.getY() + CHARACTER_HEIGHT;
            stop();
        }
    }
    //5.Intersect With Block and walk on block
    public void intersectWithBlock(){
        if (!isOnBlock){
            isOnBlock = true;
        }
    }

    public void notIntersectWithBlock() {
        if (isOnBlock) {
            isOnBlock =false;
        }
    }

    public boolean isOnBlock() {
        return isOnBlock;
    }
    //End of Block colliding

    public void trace() {
        logger.debug("x:{} y:{} vx:{} vy:{}",x,y,xVelocity,yVelocity);
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getJumpKey() {
        return jumpKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public AnimatedSprite getImageView() { return imageView; }


    public boolean isOnLadder() {
        return isOnLadder;
    }

    public boolean isCanJump() {
        return canJump;
    }

    public boolean isJumping() {
        return jumping;
    }

    public boolean isOnSpring() {
        return isOnSpring;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
