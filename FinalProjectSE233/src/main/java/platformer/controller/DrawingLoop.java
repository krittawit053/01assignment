package platformer.controller;

import platformer.model.*;
import platformer.model.Character;
import platformer.view.Platform;

import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(ArrayList<Character> characterList) throws InterruptedException {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
        }
    }
 //4.Check collision with block
    private void checkCollisionWithBlock(ArrayList<Character> characterList, ArrayList<Block> blockList) throws InterruptedException {
        for (Character main : characterList){
            for (Block block :  blockList){
                if (main.getBoundsInParent().intersects(block.getBoundsInParent())) {
                    main.intersectWithBlock();
                    main.checkCollided(block);
                    // main.traceIntersectWithBlock();
                    return;
                }
                else {
                    main.notIntersectWithBlock();
                }
            }
        }
    }

    private void checkCollisionWithTPDoor(ArrayList<Character> characterList,ArrayList<Teleport> teleportList) {

        for (Character main : characterList) {
            for (Teleport teleport : teleportList) {
                if (main.getBoundsInParent().intersects(teleport.getBoundsInParent())) {
                    main.teleport();
                    return;
                }
            }
        }
    }

    private void checkCollisionWithLadder(ArrayList<Character> characterList, ArrayList<Ladder> ladderList) {
        for (Character main : characterList){
            for (Ladder ladder : ladderList){
                if (main.getBoundsInParent().intersects(ladder.getBoundsInParent())) {
                    main.intersectsWithLadder();
                    //main.traceIntersectWithLadder();
                    return;
                }
                else {
                    main.exitLadder();
                }
            }
        }
    }

    private void checkIntersectWithSpring(ArrayList<Character> characterList, ArrayList<Spring> springList) throws  InterruptedException {
        for (Character main : characterList){
            for (Spring spring : springList){
                if (main.getBoundsInParent().intersects(spring.getBoundsInParent())) {
                    main.intersectsWithSpring();
                    return;
                }
                else{
                    main.springJump();
                }
            }
        }
    }

    private void ChangePositionKey(KeyForOpenChest keyForOpenChest, ArrayList<Character> characterList, ArrayList<Chest> chestList, Blank blank){

        for (Character character : characterList ) {
            for(Chest chest : chestList){
                if (character.getY() >= keyForOpenChest.getY() - 50 && character.getY() <= keyForOpenChest.getY() + 50) {
                    if (character.getX() >= keyForOpenChest.getX() - 50 && character.getX() <= keyForOpenChest.getX() + 50) { // Check Keep key position
                        keyForOpenChest.setTranslateX(blank.getX()+20); // Set new position of key to blank
                        keyForOpenChest.setTranslateY(blank.getY()+15);
                    }
                }
                if(character.getY() >= chest.getY()-50 && character.getY() <= chest.getY()+50) {
                    if (character.getX() >= chest.getX() - 50 && character.getX() <= chest.getX() + 50) { // If put key to Chest
                        if (keyForOpenChest.getTranslateX() == blank.getX()+20) {
                            platform.getKeyForOpenChest().setVisible(false); // Key on blank lost


                        }
                    }
                }
            }
        }
    }

    private void checkCollisionsWithSpike(ArrayList<Character> characterList,ArrayList<Spike> spikeList){

        for (Character main : characterList){
            for (Spike spike : spikeList){
                if (main.getBoundsInParent().intersects(spike.getBoundsInParent())) {
                    main.touchSpike();
                    return;
                }

            }
        }
    }

    private void paint(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.repaint();
        }
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();
            try {
                ChangePositionKey(platform.getKeyForOpenChest(),platform.getCharacterList(),platform.getChestsList(),platform.getBlank());
                checkDrawCollisions(platform.getCharacterList());
                checkCollisionWithLadder(platform.getCharacterList(),platform.getLadderList());
                checkIntersectWithSpring(platform.getCharacterList(),platform.getSpringList());
                checkCollisionWithTPDoor(platform.getCharacterList(),platform.getTeleportList());
                checkCollisionWithBlock(platform.getCharacterList(),platform.getBlockList());
                checkCollisionsWithSpike(platform.getCharacterList(),platform.getSpikeList());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            paint(platform.getCharacterList());

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
