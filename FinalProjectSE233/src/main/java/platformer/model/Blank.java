package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Blank extends Pane {
    public static final int CHARACTER_WIDTH = 64;
    public static final int CHARACTER_HEIGHT = 64;

    private int x;
    private int y;

    private Image blankImg;
    private ImageView imageView;

    public Blank(int x , int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.blankImg = new Image(getClass().getResourceAsStream("/assets/blank.png"));

        this.imageView = new ImageView(blankImg);
        this.getChildren().add(this.imageView);

        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }

}
