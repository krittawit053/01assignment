package platformer.model;

// 2.Create block class
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Block extends Pane {

    public static final int BLOCK_WIDTH = 48;
    public static final int BLOCK_HEIGHT = 48;

    private int x;
    private int y;

    private Image blockImg;

    private ImageView blockView;

    public Block(int x, int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.blockImg = new Image(getClass().getResourceAsStream("/assets/block.png"));
        this.blockView = new ImageView(blockImg);
        this.blockView.setFitWidth(BLOCK_WIDTH);
        this.blockView.setFitHeight(BLOCK_HEIGHT);

        this.getChildren().add(this.blockView);
    }

    public Image getBlockImg() {
        return blockImg;
    }

    public ImageView getBlockView() {
        return blockView;
    }

    public static int getBlockWidth() {
        return BLOCK_WIDTH;
    }

    public static int getBlockHeight() {
        return BLOCK_HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }




}





