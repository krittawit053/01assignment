package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Chest extends Pane {
    public static final int CHARACTER_WIDTH = 64;
    public static final int CHARACTER_HEIGHT = 96;
    public int Chest_width ;
    public int Chest_height;

    private int x;
    private int y;

    private Image ChestImg;
    private AnimatedSprite imageview;

    public Chest(int x,int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.ChestImg = new Image(getClass().getResourceAsStream("/assets/Chest.png"));
        this.imageview = new AnimatedSprite(ChestImg,1,1,0,0,400,500);
        Chest_width = imageview.width;
        Chest_height = imageview.height;
        this.getChildren().add(this.imageview);
        this.imageview.setFitWidth(CHARACTER_WIDTH);
        this.imageview.setFitHeight(CHARACTER_HEIGHT);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
