package platformer.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.model.*;
import platformer.model.Character;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 1300;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    private Image platformImg;
    private ArrayList<Character> characterList = new ArrayList();
    private ArrayList<Chest> chestsList = new ArrayList<>(); // Call chestList From class Chest
    private KeyForOpenChest keyForOpenChest; // Call chestList From Class KeyForOpenChest
    private Blank blank;
    private ArrayList<Teleport> teleportList = new ArrayList<>();
    private ArrayList<Spring> springList = new ArrayList<>();
    private ArrayList<Ladder> ladderList = new ArrayList<>();
    private ArrayList<Spike> spikeList = new ArrayList<>();
    private ArrayList<Block> blockList = new ArrayList<>(); //1.add block picture and block list

    private Keys keys;

    public Platform() {
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/assets/Background.png"));
        ImageView backgroundImg = new ImageView(platformImg);

        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        characterList.add(new Character(30, 30,0,0, KeyCode.A,KeyCode.D,KeyCode.SPACE,KeyCode.W));


        blockList.add(new Block(800,GROUND - 200)); // 3.Set block position 2
        blockList.add(new Block(350, GROUND - 120)); // 3.Set block position 1

        springList.add(new Spring(1000,GROUND - Spring.SPRING_HEIGHT));

        ladderList.add(new Ladder(300,GROUND - Ladder.LADDER_HEIGHT));
        ladderList.add(new Ladder(600,GROUND - Ladder.LADDER_HEIGHT));

        teleportList.add(new Teleport(390,GROUND - Teleport.TELEPORT_HEIGHT));

        keyForOpenChest = new KeyForOpenChest(280, 100); // Add Position for key item (720, 25)
        chestsList.add(new Chest(700,240));
        blank = new Blank(700,10);

        spikeList.add(new Spike(350,GROUND - Spike.SPIKE_HIGHT  ));
        spikeList.add(new Spike(130,GROUND - Spike.SPIKE_HIGHT  ));

        getChildren().add(backgroundImg);
        getChildren().addAll(chestsList);
        getChildren().add(blank);// Add blank
        getChildren().add(keyForOpenChest); // Show Key
        getChildren().addAll(characterList);
        getChildren().addAll(ladderList);
        getChildren().addAll(blockList); //1.add block picture and block list
        getChildren().addAll(teleportList);
        getChildren().addAll(spikeList);
        getChildren().addAll(springList);
    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public ArrayList<Ladder> getLadderList() {
        return ladderList;
    }

    public ArrayList<Spring> getSpringList() {
        return springList;
    }

    public Keys getKeys() {
        return keys;
    }

    public ArrayList<Block> getBlockList() {
        return blockList;
    }

    public ArrayList<Chest> getChestsList(){
        return chestsList;
    }

    public KeyForOpenChest getKeyForOpenChest(){
        return keyForOpenChest;
    } // get key item

    public ArrayList<Teleport> getTeleportList() {
        return teleportList;
    }

    public Blank getBlank(){
        return blank;
    }

    public ArrayList<Spike> getSpikeList() {
        return spikeList;
    }

}

