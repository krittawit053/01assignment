import de.saxsys.javafx.test.JfxRunner;
import javafx.scene.input.KeyCode;
import org.junit.runner.RunWith;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.model.Character;
import platformer.model.Teleport;
import platformer.view.Platform;

import java.lang.reflect.Method;

@RunWith(JfxRunner.class)
public class CharacterTest {

    private Character floatingCharacter;
    private Character onGroundCharacter;
    private DrawingLoop drawingLoopUnderTest;
    private Platform platformUnderTest;
    private GameLoop gameLoopUnderTest;
    private Teleport teleport;
    private Method checkCollisionsWithTPDoor;
    private Method updateMethod;
    private Method checkDrawingCollisionMethod;
    private Method paintMethod;

    private int x = 30;
    private int y = 30;
    private int offsetX = 0;
    private int offsetY = 0;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;

    public CharacterTest() {
        this.leftKey = KeyCode.A;
        this.rightKey = KeyCode.D;
        this.upKey = KeyCode.W;
    }


}
