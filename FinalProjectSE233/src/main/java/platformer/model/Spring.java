package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;


public class Spring extends Pane {

    public static final int SPRING_WIDTH = 24;
    public static final int SPRING_HEIGHT = 6;

    private int x;
    private int y;

    private Image springImg;

    private ImageView springView;

    public Spring(int x, int y) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);

        this.springImg = new Image(getClass().getResourceAsStream("/assets/spring.png"));
        this.springView = new ImageView(springImg);
        this.springView.setFitWidth(SPRING_WIDTH);
        this.springView.setFitHeight(SPRING_HEIGHT);

        this.getChildren().add(this.springView);
    }

    public Image getLadderImg() {
        return springImg;
    }

    public ImageView getLadderView() {
        return springView;
    }

    public static int getLadderWidth() {
        return SPRING_WIDTH;
    }

    public static int getLadderHeight() {
        return SPRING_HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}